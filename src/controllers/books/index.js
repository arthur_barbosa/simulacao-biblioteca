const bookService = require('../../models/books');
module.exports = {
    register,
    getAll,
    getById,
    update,
    delete: _delete
};

function register(req, res, next) {
    bookService
        .create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    bookService
        .getAll()
        .then(profiles => res.json(profiles))
        .catch(err => next(err));
}

function getById(req, res, next) {
    bookService
        .getById(req.params.id)
        .then(profile => (profile ? res.json(profile) : res.sendStatus(404)))
        .catch(err => next(err));
}

function update(req, res, next) {
    bookService
        .update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    bookService
        .delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}
