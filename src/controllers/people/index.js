const personService = require('../../models/people');
module.exports = {
    register,
    getAll,
    getById,
    update,
    delete: _delete,
    loan_books
};

function register(req, res, next) {
    personService
        .create(req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function getAll(req, res, next) {
    personService
        .getAll()
        .then(profiles => res.json(profiles))
        .catch(err => next(err));
}

function getById(req, res, next) {
    personService
        .getById(req.params.id)
        .then(profile => (profile ? res.json(profile) : res.sendStatus(404)))
        .catch(err => next(err));
}

function update(req, res, next) {
    personService
        .update(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function _delete(req, res, next) {
    personService
        .delete(req.params.id)
        .then(() => res.json({}))
        .catch(err => next(err));
}

function loan_books(req, res, next) {
    personService
        .loan_books(req.params.id, req.body)
        .then(() => res.json({}))
        .catch(err => next(err));
}
