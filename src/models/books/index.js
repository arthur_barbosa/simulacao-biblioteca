const db = require('../../databases');
const Book = db.Book;
const Enum = require('enum');

const status = new Enum(['DISPONIVEL', 'EMPRESTADO']);

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    getByName,
    status
};

async function getAll() {
    return await Book.find();
}

async function getAllSelectName() {
    return await Book.find().select('name -_id');
}

async function getById(id) {
    return await Book.findById(id);
}

async function getByName(str) {
    console.log(str);
    return await Book.find({ name: str }).select('name');
}

async function create(bookParam) {
    // validate
    if (await Book.findOne({ name: bookParam.name })) {
        throw 'Book Name "' + bookParam.name + '" is already taken';
    }

    const book = new Book(bookParam);
    book.status = status.DISPONIVEL.value;
    book.created_at = Date.now();
    book.updated_at = Date.now();

    // save book_del
    await book.save();
}

async function update(id, bookParam) {
    const book = await Book.findById(id);

    // validate
    if (!book) throw 'User not found';
    if (book.name !== bookParam.name && (await Book.findOne({ name: bookParam.name }))) {
        throw 'Book name "' + bookParam.name + '" is already taken';
    }

    // copy bookParam properties to book
    Object.assign(book, bookParam);

    await book.save();
}

async function _delete(id) {
    await Book.findByIdAndRemove(id);
}
