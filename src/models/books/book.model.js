const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const BookSchema = new Schema({
  name: { type: String, required: true, unique: true },
  status: { type: Number, required: false },
  created_at: Date,
  updated_at: Date
});

BookSchema.set('toJSON', { virtuals: true });
module.exports = mongoose.model('Book', BookSchema);
