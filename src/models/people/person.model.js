const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PersonSchema = new Schema({
  name: { type: String, required: true, unique: true },
  borrowed_books: { type: Array, required: false },
  created_at: Date,
  updated_at: Date
});

PersonSchema.set('toJSON', { virtuals: true });
module.exports = mongoose.model('Person', PersonSchema);
