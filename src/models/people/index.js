const db = require('../../databases');
const Person = db.Person;
const Book = db.Book;
const bookService = require('../../models/books');

module.exports = {
    getAll,
    getById,
    create,
    update,
    delete: _delete,
    loan_books
};

async function getAll() {
    return await Person.find();
}

async function getById(id) {
    return await Person.findById(id);
}

async function create(personParam) {
    // validate
    if (await Person.findOne({ name: personParam.name })) {
        throw 'Person Name "' + personParam.name + '" is already taken';
    }

    const person = new Person(personParam);

    person.created_at = Date.now();
    person.updated_at = Date.now();

    // save person_del
    await person.save();
}

async function update(id, personParam) {
    const person = await Person.findById(id);

    // validate
    if (!person) throw 'User not found';
    if (person.name !== personParam.name && (await Person.findOne({ name: personParam.name }))) {
        throw 'Person name "' + personParam.name + '" is already taken';
    }

    // copy personParam properties to person
    Object.assign(person, personParam);

    await person.save();
}

async function _delete(id) {
    await Person.findByIdAndRemove(id);
}

async function loan_books(id, personParam) {
    const person = await Person.findById(id);
    //validate user exist
    if (!person) throw 'User not found';
    //validate if book exist
    let books_to_update = [];

    for (let book of personParam.books) {
        books = await bookService.getByName(book);
        if (books.length > 0) {
            books_to_update.push({ id: books[0]._id, status: bookService.status.EMPRESTADO.value });
        }
        if ((await bookService.getByName(book)).length === 0) {
            throw 'Book not exist';
        }
    }

    console.log('sai da qui');
    console.log(books_to_update);
    for (let item of books_to_update) {
        await bookService.update(item.id, { status: item.status });
    }

    Object.assign(person, { borrowed_books: personParam.books });
    await person.save();
}
