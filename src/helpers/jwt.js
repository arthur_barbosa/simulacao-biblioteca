const expressJwt = require('express-jwt');
const userService = require('../models/users');
require('dotenv').load();

function jwt() {
    const secret = process.env.SECRET;
    console.log(secret);
    return expressJwt({ secret, isRevoked }).unless({
        path: [
            // public routes that don't require authentication
            '/api/v1/users/authenticate',
            '/api/v1/users/register',
            '/api/v1/users/reauthenticate'
        ]
    });
}

async function isRevoked(req, payload, done) {
    const user = await userService.getById(payload.sub);
    // revoke token if user no longer exists
    if (!user) {
        return done(null, true);
    }
    done();
}

module.exports = jwt;
