const express = require('express');
const router = express.Router();
const controller_books = require('../controllers/books');

router.post('/register', controller_books.register);
router.get('/', controller_books.getAll);
router.get('/:id', controller_books.getById);
router.put('/:id', controller_books.update);
router.delete('/:id', controller_books.delete);

module.exports = router;
