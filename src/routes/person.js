const express = require('express');
const router = express.Router();
const controller_people = require('../controllers/people');

router.post('/register', controller_people.register);
router.get('/', controller_people.getAll);
router.get('/:id', controller_people.getById);
router.put('/:id', controller_people.update);
router.delete('/:id', controller_people.delete);
router.put('/:id/loan', controller_people.loan_books);
module.exports = router;
